package org.samak.intellij.plugins;

import org.samak.intellij.plugins.eliza.ElizaMain;

import java.io.InputStream;

public class ElizaTalker implements Talker {
	private static final String SCRIPT_PATH = "/talkers/eliza/script.txt";
	private final ElizaMain eliza;

	public ElizaTalker() {
		InputStream script = getClass().getClassLoader().getResourceAsStream(SCRIPT_PATH);

		eliza = new ElizaMain();
		int res = eliza.readScript(script);
		if (res != 0) {
			throw new RuntimeException("Cannot connect to Eliza");
		}

	}

	public String processInput(String input) {
		return eliza.processInput(input);
	}
}
