package org.samak.intellij.plugins;

import java.awt.Component;

public interface Response {

	public String getResponse();

	public String getName();

	public void style(Component component);


}
