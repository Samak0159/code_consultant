package org.samak.intellij.plugins;

import java.awt.Component;
import java.awt.Font;

public class TalkerResponse implements Response {

	private String response;

	public TalkerResponse(String response) {
		this.response = response;
	}

	public String getResponse() {
		return response;
	}

	public String getColor() {
		return "BLUE";
	}

	public String getName() {
		return "Me";
	}

	public void style(Component component) {
		component.setFont(component.getFont().deriveFont(Font.BOLD));
	}
}
